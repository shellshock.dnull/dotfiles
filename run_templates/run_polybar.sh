#!/bin/bash
export DISPLAY={{ var.display }}

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch Polybar, using default config location {{ template_dest }}
{% for bar in var.bars %}
polybar {{ bar }} -r -c {{ templates.polybar.config }} &
{% endfor %}

echo "Polybar launched..."
