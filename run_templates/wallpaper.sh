#!/bin/bash
export DISPLAY={{ var.display }}
feh --bg-fill {{ var.wallpaper }}
