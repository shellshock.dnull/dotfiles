# Run systemd --user daemons

```
export DAEMON_NAME=synergy

cd ~/.config/systemd/user/
cp ~/bin/daemons/${DAEMON_NAME} .

ln -s /home/${USER}/bin/daemons/${DAEMON_NAME}.service .

systemctl --user enable ${DAEMON_NAME}
systemctl --user start ${DAEMON_NAME}
systemctl --user status ${DAEMON_NAME}
```

