" PLUGINS
call plug#begin('~/.vim/plugged')

{% for plugin in var.plugins %}
Plug {{ plugin }}
{% endfor %}

call plug#end()
