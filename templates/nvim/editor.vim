" EDITOR
set mouse=a " Use mouse
set number relativenumber " hybrid line number
set cursorline " horizontal line
set cursorcolumn " highlight the cursor screen line "
highlight CursorColumn cterm=underline  term=underline

" use system clipboard
set clipboard=unnamed,unnamedplus
set autoread

" syntax on
set encoding=utf-8
set tabstop=2
set shiftwidth=2
set expandtab
" Tab in yaml is 2 spaces
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" spellCheck on
set spelllang=en
set spellfile=$HOME/dNull/dotfiles/files/nvim/en.utf-8.add

set list " Show special chars
set listchars=tab:▸▸ " Show tabs as >-
" set listchars=space:␣

" AutoCh Dir of currect file - usefull for TF
set autochdir

" Terraform
let g:terraform_fmt_on_save=1
let g:terraform_align=1

" AirLine - tabline enabled
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

" Themes
let g:airline_theme='distinguished'

" Ranger - disable border
" let g:rnvimr_draw_border = 0
let g:rnvimr_enable_bw = 1

" YAMLFormat
" fix indent
let g:yaml_formatter_indent_collection=0

" Incubator
" let g:minimap_left=1
autocmd CursorMoved *.yml echo localorie#expand_key()
autocmd CursorMoved *.yaml echo localorie#expand_key()

