function! autoload#after() abort
  " autocmd CursorMoved *.yaml echo localorie#expand_key()
  " autocmd CursorMoved *.yml echo localorie#expand_key()
  " autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
  let g:rnvimr_enable_bw = 1
  let g:rnvimr_ex_enable = 1
  let g:terraform_align=1
  let g:terraform_fmt_on_save=1
  let g:yaml_formatter_indent_collection=0
  nmap <C-f> :RnvimrToggle<CR>
  nnoremap <C-H> <C-W><C-H>
  nnoremap <C-J> <C-W><C-J>
  nnoremap <C-K> <C-W><C-K>
  nnoremap <C-L> <C-W><C-L>
  nnoremap <C-W><C-W> :MaximizerToggle<CR>
  nnoremap <silent> <Leader>ga :Git add %<CR>
  nnoremap <silent> <Leader>gl :Git log<CR>
  nnoremap <silent> <Leader>gp :Git -c push.default=current push<CR>
  nnoremap <silent> <Leader>gs :Git status<CR>
  set autoread
  set clipboard=unnamed,unnamedplus
  set cursorcolumn
  set cursorline
  set encoding=utf-8
  set expandtab
  set list
  set listchars=tab:▸▸ 
  set mouse=a
  set nobackup
  set noswapfile
  set nowritebackup
  set number relativenumber
  set shiftwidth=2
  set shortmess+=c
  set spellfile=$HOME/dNull/dotfiles/files/nvim/en.utf-8.add
  set spelllang=en
  set splitbelow
  set splitright
  set tabstop=2
  tnoremap <ESC> <C-\><C-N>
endfunction
